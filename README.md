# Kafka Spring Boot Setup

This guide outlines the steps to set up Apache Kafka with a Spring Boot application.

Prerequisites

- Apache Kafka installed on your machine or accessible via a cloud service.
- Java Development Kit (JDK) installed on your machine.
- Maven dependencies.

Kafka Spring Boot Project

This project demonstrates how to integrate Apache Kafka with a Spring Boot application for messaging purposes.

Pre-requisites

Before running this project, ensure that you have the following installed:

- Java Development Kit (JDK) 17 or higher
- Apache Kafka installed and running (local setup or accessible via a cloud service)
- Apache Maven installed


Getting Started

Follow these steps to run the project:

1.Clone the Repository. 
2.Navigate to the Project Directory.
3.Build the Project
4.Run the Application


## Usage

Once the application is running, it will expose REST endpoints for producing and consuming messages to/from Kafka topics. Here are the available endpoints:

- **GET /api/message/send**
- Description: Send a message to a Kafka topic.
- Request body:
 ```json
 {
   "topic": "example-topic",
   "message": "Hello, Kafka!"
 }
 ```

- **GET /api/message/receive**
- Description: Consume a message from a Kafka topic.












